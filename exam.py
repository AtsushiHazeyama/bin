# -*- coding: utf-8 -*-
import os
from tkinter import *
from tkinter import ttk
from tkinter.ttk import *
from tkinter.font import Font
from tkinter import messagebox
from tkinter import filedialog
from os.path import expanduser

# DIR選択ダイアログ設定
def select_click():
    exec_dir_entry.delete(0, END)
    ini_dir = expanduser("~")           
    ret = filedialog.askdirectory(initialdir=ini_dir, title='dir choose', mustexist = True)
#    messagebox.showinfo('選択したディレクトリ',ret)
    exec_dir_entry.insert(0, ret)


# 設定内容クリア
def clear_click():
    exec_dir_entry.delete(0, END)
    undel_wd_entry.delete(0, END)
    msg.delete(1.0, END)
    msg['fg'] = 'black'
    msg.insert(1.0, "no message!")


# HELP表示
def help_click():
    msg.delete(1.0, END)
    # Text(msg)内の文字位置調整要
    #   参考:https://www.shido.info/py/tkinter12.html
    msg['fg'] = 'blue'
    prt_wd =  'name : exrm\n'
    prt_wd += 'function : Exclusive Removal Tool (排他的ファイル[ディレクトリ]削除ツール)\n'
    prt_wd += 'usage :\n'
    prt_wd += '    Exec directory             : 指定ディレクトリ以下が処理対象となります。\n'
    prt_wd += '    not removed words     : 削除しないファイル[ディレクトリ]名に含まれる文字列を指定して下さい。\n'
    prt_wd += '                                         : カンマ区切りで複数指定できます。\n'
    prt_wd += '    Recursive processing : 下位階層のディレクトリも変換対象とします。\n'
    msg.insert(END, prt_wd)


# ファイル[ディレクトリ]削除処理
def remove_click():
    msg.delete(1.0, END)
    msg['fg'] = 'black'
    sps = ""
    dir_flg = 1
    dir_wd = exec_dir_entry.get()
    udl_str = undel_wd_entry.get()
    prt_wd = ""
    msg['fg'] = 'blue'
    if dir_wd == "":
        prt_wd += "'Exec directory' を指定して下さい。\n"
    if udl_str == "":
        prt_wd += "'not removed words' を指定して下さい。\n"
    if prt_wd != "":
        msg.insert(END, prt_wd)
    else:
        udl_str = udl_str.replace(" ", "")                 # 空白を削除
        udl_wds = udl_str.split(',')                       # 文字列をカンマで分割
        os.chdir(dir_wd)                                   # 処理DIRへ移動
        dwndir(sps, udl_wds)                               # 再起処理サブルーチン呼出


# 再起処理サブルーチン
def dwndir(sps, udl_wds):
    sps = sps + "    "
    items = ""
    items = [f.name for f in os.scandir() if not f.name.startswith('.')] # File or DIR名のみ抽出
    del_flg = 1
    dir_wd = os.getcwd()
    del_nm = ""
    prt_wd = sps + ' => ' + dir_wd + '\n'
    msg.insert(END, prt_wd)
    for item in items:
        del_nm = os.path.join(dir_wd, item)
        del_flg = 1
        for udl_wd in udl_wds:
            if udl_wd in item:                             # File or DIR名(pathは除く)に削除対象wordが含まれるか?
                del_flg = 0
                break
        if del_flg == 1:                                   # 削除対象
            if os.path.isdir(del_nm):                      # 処理対象がDIR
                if bln0.get():                             # DIR再帰処理フラグ有
                    os.chdir(del_nm)
                    dwndir(sps, udl_wds)                   # 再起処理サブルーチン呼出
                    os.chdir('..')
                    try:
                        os.rmdir(del_nm)                   # DIR削除(中身があれば削除されない => 例外処理へ)
                        prt_wd = sps + 'rmdir      ' + del_nm + '\n'
                    except OSError as e:
                        prt_wd = sps + 'leave_d in ' + del_nm + '\n'
                        pass
                else:
                    prt_wd = sps + 'leave_d fg ' + del_nm + '\n'
            else:
                prt_wd = sps + 'remove     ' + del_nm + '\n'
                os.remove(del_nm)
        else:
            prt_wd = sps + 'leave_f ne ' + del_nm + '\n'
        msg.insert(END, prt_wd)
    prt_wd = sps + ' <= ' + dir_wd + '\n'
    msg.insert(END, prt_wd)


if __name__ == '__main__':
    root = Tk()
    root.title('Exclusive removal tool [Exrm]')
    root.resizable(False, False)

##############
### Frame0 ###
##############
    frame0 = ttk.Frame(root, padding=10)
    frame0['relief'] = 'sunken'
    frame0.grid()
    
##############
### Frame1 ###
##############

    frame1 = ttk.Frame(frame0, padding=(2, 5))
    frame1.grid(row=0, column=0, sticky=W)
    
    label1 = ttk.Label(frame1, text='Exec directory', width=10, padding=(5, 2))
    label1.pack(side=LEFT)
    
    # Execution directory Entry
    exec_dir = StringVar()
    exec_dir_entry = ttk.Entry(
        frame1, 
        textvariable=exec_dir, 
        width=34 )
    exec_dir_entry.pack(side=LEFT)

    # Directory Select Button
    button0 = ttk.Button(frame1, text='Select', command=select_click)
    button0.pack(side=LEFT)
    
##############
### Frame2 ###
##############

    frame2 = ttk.Frame(frame0, padding=(2, 5))
    frame2.grid(row=1, column=0, sticky=W)

    # Not Removed words Lavel
    label2 = ttk.Label(frame2, text='not removed words', width=13, padding=(5, 2))
    label2.grid(row=0, column=0, sticky=W)

    # Not Removed words
    undel_wd = StringVar()
    undel_wd_entry = ttk.Entry(
        frame2, 
        textvariable=undel_wd, 
        width=38)
    undel_wd_entry.grid(row=0, column=1, sticky=W)

    # Recursive Check
    bln0 = BooleanVar()
    bln0.set(True)
    rec_chk = ttk.Checkbutton(frame2, variable=bln0, text='Recursive processing')
    rec_chk.grid(row=1, column=1, sticky=W)

##############
### Frame3 ###
##############

    frame3 = ttk.Frame(frame0, padding=(2, 5))
    frame3.grid(row=2, column=0, sticky=(W, E))
    
    # Message Entry
    label4 = ttk.Label(frame3, text='Processing message', padding=(5, 2))
    label4.grid(row=0, column=0, sticky=W)
    
    # Text
    f = Font(family='Helvetica', size=11)
    v1 = StringVar()
    msg = Text(frame3, width=85)
    msg.configure(font=f)
    msg.grid(row=1, column=0, sticky=(N, W, S, E))
    prt_wd = 'Exclusive removal tool\n'
    msg.insert(1.0, prt_wd)
    msg['fg'] = 'red'
    prt_wd =  '\n!!注意!!\n'
    prt_wd += '  このツールは"not removed words"に指定された文字列を"含まない"ファイル[ディレクトリ]を\n'
    prt_wd += '  問答無用で削除します!!\n'
    prt_wd += '  使用方法は[Help]ボタンで表示されます。'
    msg.insert(END, prt_wd)

    # Scrollbar
    scrollbar = ttk.Scrollbar(
        frame3, 
        orient=VERTICAL, 
        command=msg.yview)
    msg['yscrollcommand'] = scrollbar.set
    scrollbar.grid(row=1, column=0, sticky=(N, S, E))

### ##############
### ### Frame4 ###
### ##############
# MacでTKinter標準?の機能でボタンのAppearance変更は難しい...ので、標準のまま。
# Linux,Windows移植時の影響確認が面倒くさいから。

    frame4 = ttk.Frame(frame0, padding=(0, 5))
    frame4.grid(row=3, column=0, sticky=W)
    
    button1 = ttk.Button(frame4, text='Help', command=help_click)
    button1.pack(anchor="nw", side="left")
    
    button2 = ttk.Button(frame4, text='ExRemove', command=remove_click)
    button2.pack(anchor="nw", side="left")
    
    button4 = ttk.Button(frame4, text='Clear', command=clear_click)
    button4.pack(anchor="nw", side="left")
    
    label5 = ttk.Label(frame4, text='', width=15, padding=(5, 2)) # Button5右寄せの為のダミー
    label5.pack(anchor="ne", side="left")
    
    button5 = ttk.Button(frame4, text='Exit', command=quit)
    button5.pack(anchor="ne", side="left")


    root.attributes("-topmost", True)                      # ウィンドウを最前面へ(この段階では最前面固定)
    root.attributes("-topmost", False)                     # ウィンドウを最前面固定解除
    root.mainloop()