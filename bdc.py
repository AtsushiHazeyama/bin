# -*- coding: utf-8 -*-
import os
import tkinter.simpledialog as simpledialog
from tkinter import *
from tkinter import ttk
from tkinter.font import Font
from tkinter import messagebox
from tkinter import filedialog
from os.path import expanduser

# Binary Entryコピー
def copyB():
    wk_txt = bin_val_entry.get()
    root.clipboard_clear()
    root.clipboard_append(wk_txt)                          # ここがクリップボードへのコピーをする処理


# Decimal Entryコピー
def copyD():
    wk_txt = dec_val_entry.get()
    root.clipboard_clear()
    root.clipboard_append(wk_txt)                          # ここがクリップボードへのコピーをする処理


# Decimal Entryクリア
def callbackB(eventB):
    dec_val_entry.delete(0, END)
    dic_val_entry.config(state="disable")
    sep_chk.config(state="disable")


# Binary Entryクリア
def callbackD(eventD):
    bin_val_entry.delete(0, END)
    dic_val_entry.config(state="enable")
    sep_chk.config(state="enable")


# Help表示
def help_click():
#    msg.delete(1.0, END)
#    msg['fg'] = 'blue'
    wk_txt =  'name : bdc\n'
    wk_txt += '2進数<=>10進数変換ツール\n'
    wk_txt += '  Binary  value : 2進数値を入力して下さい。\n'
    wk_txt += '  Decimal value : 10進数値を入力して下さい。\n'
    wk_txt += '  Digit designation : 出力桁数を指定して下さい。\n'
    wk_txt += '  Digit separator : 桁区切を挿入する場合にチェックして下さい。\n'
#    msg.insert(END, wk_txt)
    messagebox.showinfo('help',wk_txt)


# 2進数<=>10進数変換
def convert_click():
#    msg.delete(1.0, END)
#    sps = ""
    dir_flg = 1
    bin_s = bin_val_entry.get()
    dec_s = dec_val_entry.get()
    wk_txt = ""
    # 2進数と10進数変換のどちらかしか入力できないので、片側判定でOK
    if bin_s != "":
        bin2dec(bin_s)
    elif dec_s != "":
        dec2bin(dec_s)
    else: # 但し、両方空欄はあり得る
        wk_txt += '"Binary  value"か"Decimal value"の\nどちらかに入力して下さい。'
        messagebox.showwarning('Warning',wk_txt)
#        messagebox.showerror('Warning',wk_txt)


# 2進数 => 10進数変換
def bin2dec(bin_s):
    bin_s = bin_s.replace(',','')                          # 桁区切","削除
    bin_l = list(bin_s)                                    # string => list
    ii = 0
    dec_d = 0
    for bin_c in reversed(bin_l):                          # list => 降順 Loop
        if bin_c == "1":
            dec_d += 2 ** ii
        ii += 1
    dec_val_entry.insert(END, str(dec_d))
    

# 10進数変換 => 2進数
def dec2bin(dec_s):
    dec_d = int(dec_s)
    dic_s = dic_val_entry.get()
    dic_d = 0
    if dic_s != "":
        dic_d = int(dic_s)
    sno_d = 0
    bin_s = ""
    while dec_d >= 1:
        sno_d += 1
        bin_s = str(dec_d % 2) + bin_s
        dec_d = dec_d // 2                                 # "//":整数部取り出し
        if (bln0.get() & ((sno_d % 3) == 0) & (dec_d > 1)):
            bin_s = "," + bin_s                            # 桁区切"､"付加
        if dic_d > 0:
            dic_d -= 1
    for ii in range(dic_d):                                # 先頭"0"付加
        sno_d += 1
        bin_s = "0" + bin_s
        if (bln0.get() & ((sno_d % 3) == 0) & (ii < dic_d-1)):
            bin_s = "," + bin_s                            # 桁区切"､"付加
    bin_val_entry.insert(END, bin_s)


if __name__ == '__main__':
    root = Tk()
    root.title('Binary <-> Decimaal Conversion Tool [bdc]')
    root.resizable(False, False)

##############
### Frame0 ###
##############
    frame0 = ttk.Frame(root, padding=10)
    frame0['relief'] = 'sunken'
    frame0.grid()
    
##############
### Frame1 ###
##############

    frame1 = ttk.Frame(frame0, padding=(2, 5))
    frame1.grid(row=0, column=0, sticky=W)
    
    # Binary value 
    label1 = ttk.Label(frame1, text='Binary  value', width=10, padding=(5, 2))
    label1.grid(row=0, column=0, sticky=W)
    
    bin_val = StringVar()
    bin_val_entry = ttk.Entry(
        frame1, 
        textvariable=bin_val, 
        width=50 )
    bin_val_entry.bind("<Button-1>", callbackB)
    bin_val_entry.grid(row=0, column=1, sticky=W)
    
    btn_copyB = ttk.Button(frame1, text='Copy', command=copyB)
    btn_copyB.grid(row=0, column=2, sticky=W)

    # Decimal value 
    label2 = ttk.Label(frame1, text='Decimal value', width=10, padding=(5, 2))
    label2.grid(row=1, column=0, sticky=W)
    
    dec_val = StringVar()
    dec_val_entry = ttk.Entry(
        frame1, 
        textvariable=dec_val, 
        width=50)
    dec_val_entry.bind("<Button-1>", callbackD)
    dec_val_entry.grid(row=1, column=1, sticky=W)

    btn_copyD = ttk.Button(frame1, text='Copy', command=copyD)
    btn_copyD.grid(row=1, column=2, sticky=W)

##############
### Frame2 ###
##############

    frame2 = ttk.Frame(frame0, padding=(2, 5))
    frame2.grid(row=1, column=0, sticky=W)
    
    # Decimal value 
    label3 = ttk.Label(frame2, text='Digit designation', width=12, padding=(5, 2))
    label3.grid(row=0, column=0, sticky=W)
    
    dic_val = StringVar()
    dic_val_entry = ttk.Entry(
        frame2, 
        textvariable=dic_val, 
        width=5)
    dic_val_entry.grid(row=0, column=1, sticky=W)

    # Separator Check
    bln0 = BooleanVar()
    bln0.set(False)
    sep_chk = ttk.Checkbutton(frame2, variable=bln0, text='Digit separator')
    sep_chk.grid(row=0, column=2, sticky=W)

    # Convert Button
    btn_conv = ttk.Button(frame2, text='Convert', command=convert_click)
    btn_conv.grid(row=0, column=3, padx=40)                # padxでボタン間隔を調整

    # Help Button
    btn_help = ttk.Button(frame2, text='Help', command=help_click)
    btn_help.grid(row=0, column=4, padx=5)
    
    # Exit Button
    btn_exit = ttk.Button(frame2, text='Exit', command=quit)
    btn_exit.grid(row=0, column=5, sticky=(W,E))

    root.attributes("-topmost", True)                      # ウィンドウを最前面へ(この段階では最前面固定)
    root.attributes("-topmost", False)                     # ウィンドウを最前面固定解除
    root.mainloop()