#! /usr/bin/perl

#_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# name     : rem.pl
# function : rename of files
# method   : rem.pl [-R] before_word after_word
#_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

my $dir_fg = 0;
my $sps = "";
my $dir = `pwd`;
chop $dir;
if (($#ARGV == 2) && ($ARGV[0] =~ /-R/)) {
	$dir_fg = 1;
	$bfo_wd = $ARGV[1];
	$aft_wd = $ARGV[2];
} elsif ($#ARGV == 1) {
	$bfo_wd = $ARGV[0];
	$aft_wd = $ARGV[1];
} else {
	print "_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/\n";
	print "_/ name		: rem.pl\n";
	print "_/ function	: rename of files\n";
	print "_/ usae		: rem.pl [-R] before_word after_word\n";
	print "_/			: [-R]			: Option. The directories under it will also be changed.\n";
	print "_/			: before_word   : String to be converted.\n";
	print "_/			: after_word	: Character string after conversion.\n";
	print "_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/\n";
	exit;
}
dwndir($bfo_wd, $aft_wd, $sps);								# 再帰処理サブルーチン呼び出し
exit;
	
sub dwndir{ 
 	my ($bfo_wd, $aft_wd, $sps) = @_;
	$sps .= "    ";
 	my @files = `ls`;
	my $dir = `pwd`;
	chop $dir;
	print "\n$sps=> $dir\n";
 	foreach my $bfo_nm (@files) {
		chop $bfo_nm;
		my $aft_nm = $bfo_nm;
 		$aft_nm =~ s/$bfo_wd/$aft_wd/g;
		if ((-d $bfo_nm) && ($dir_fg == 1)) {
			chdir $bfo_nm;
			dwndir($bfo_wd, $aft_wd, $sps);					# 再帰処理サブルーチン呼び出し
			chdir "..";
		}
	 	if ($bfo_nm =~ /$bfo_wd/) {
 			rename ($bfo_nm, $aft_nm);
 			print "$sps	:$bfo_nm => $aft_nm\n";
 		}
	}
	print "$sps<= $dir\n\n";
 }